<?php

namespace App\DataFixtures;

use App\Entity\Disponibilitees;
use App\Entity\Enseignants;
use App\Entity\Interventions;
use App\Entity\Matieres;
use App\Entity\Planning;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class EnseignantsFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $es1 = new Enseignants();
        $es1->setNom("Da Costa");
        $es1->setPrenom("Guillaume");

        $es = new Enseignants();
        $es->setNom("Anton");
        $es->setPrenom("Armand");

        $manager->persist($es);
        $manager->persist($es1);

        $disp = new Disponibilitees();
        $disp->setEnseignant($es1);
        $disp->setDate(new \DateTime("2019-11-25"));
        $disp->setHeureDebut(new \DateTime("08:00"));
        $disp->setHeureFin(new \DateTime("18:00"));
        $disp->setCommentaire("Bonjour,\nJe ne serai pas disponible à cette date car j'ai cours dans un autre établissement.\nCdt,");

        $disp1 = new Disponibilitees();
        $disp1->setEnseignant($es1);
        $disp1->setDate(new \DateTime("2019-11-29"));
        $disp1->setHeureDebut(new \DateTime("08:00"));
        $disp1->setHeureFin(new \DateTime("12:00"));
        $disp1->setCommentaire("Bonjour,\nJe ne peux pas venir pour cette matinée car j'ai un rdv important\nCdt,");

        $disp2 = new Disponibilitees();
        $disp2->setEnseignant($es1);
        $disp2->setDate(new \DateTime("2019-11-29"));
        $disp2->setHeureDebut(new \DateTime("08:00"));
        $disp2->setHeureFin(new \DateTime("12:00"));
        $disp2->setCommentaire("Bonjour,\nJe ne peux pas venir pour cette matinée car j'ai un rdv important\nCdt,");

        $disp3 = new Disponibilitees();
        $disp3->setEnseignant($es1);
        $disp3->setDate(new \DateTime("2019-11-29"));
        $disp3->setHeureDebut(new \DateTime("08:00"));
        $disp3->setHeureFin(new \DateTime("12:00"));
        $disp3->setCommentaire("Bonjour,\nJe ne peux pas venir pour cette matinée car j'ai un rdv important\nCdt,");

        $disp4 = new Disponibilitees();
        $disp4->setEnseignant($es1);
        $disp4->setDate(new \DateTime("2019-11-29"));
        $disp4->setHeureDebut(new \DateTime("08:00"));
        $disp4->setHeureFin(new \DateTime("12:00"));
        $disp4->setCommentaire("Bonjour,\nJe ne peux pas venir pour cette matinée car j'ai un rdv important\nCdt,");

        $disp5 = new Disponibilitees();
        $disp5->setEnseignant($es1);
        $disp5->setDate(new \DateTime("2019-11-29"));
        $disp5->setHeureDebut(new \DateTime("08:00"));
        $disp5->setHeureFin(new \DateTime("12:00"));
        $disp5->setCommentaire("Bonjour,\nJe ne peux pas venir pour cette matinée car j'ai un rdv important\nCdt,");

        $disp6 = new Disponibilitees();
        $disp6->setEnseignant($es1);
        $disp6->setDate(new \DateTime("2019-11-29"));
        $disp6->setHeureDebut(new \DateTime("08:00"));
        $disp6->setHeureFin(new \DateTime("12:00"));
        $disp6->setCommentaire("Bonjour,\nJe ne peux pas venir pour cette matinée car j'ai un rdv important\nCdt,");
        
        $disp7 = new Disponibilitees();
        $disp7->setEnseignant($es1);
        $disp7->setDate(new \DateTime("2019-11-29"));
        $disp7->setHeureDebut(new \DateTime("08:00"));
        $disp7->setHeureFin(new \DateTime("12:00"));
        $disp7->setCommentaire("Bonjour,\nJe ne peux pas venir pour cette matinée car j'ai un rdv important\nCdt,");

        $disp8 = new Disponibilitees();
        $disp8->setEnseignant($es1);
        $disp8->setDate(new \DateTime("2019-11-29"));
        $disp8->setHeureDebut(new \DateTime("08:00"));
        $disp8->setHeureFin(new \DateTime("12:00"));
        $disp8->setCommentaire("Bonjour,\nJe ne peux pas venir pour cette matinée car j'ai un rdv important\nCdt,");

        $disp9 = new Disponibilitees();
        $disp9->setEnseignant($es1);
        $disp9->setDate(new \DateTime("2019-11-29"));
        $disp9->setHeureDebut(new \DateTime("08:00"));
        $disp9->setHeureFin(new \DateTime("12:00"));
        $disp9->setCommentaire("Bonjour,\nJe ne peux pas venir pour cette matinée car j'ai un rdv important\nCdt,");

        $manager->persist($disp);
        $manager->persist($disp1);
        $manager->persist($disp2);
        $manager->persist($disp3);
        $manager->persist($disp4);
        $manager->persist($disp5);
        $manager->persist($disp6);
        $manager->persist($disp7);
        $manager->persist($disp8);
        $manager->persist($disp9);

        $matiere1 = new Matieres();
        $matiere1->setNomMatiere('Mathématiques');

        $matiere2 = new Matieres();
        $matiere2->setNomMatiere('Physique');

        $manager->persist($matiere1);
        $manager->persist($matiere2);

        $inter1 = new Interventions();
        $inter1->setMatiere($matiere1);
        $inter1->setEnseignant($es1);
        $inter1->setNomInter('Les équations différentielles');

        $inter2 = new Interventions();
        $inter2->setMatiere($matiere2);
        $inter2->setEnseignant($es);
        $inter2->setNomInter('Les transformés de fourier');

        $manager->persist($inter1);
        $manager->persist($inter2);

        $plan = new Planning();
        $plan->setDate(new \DateTime("2019-12-01"));
        $plan->setInterMatin($inter1);
        $plan->setInterAprem($inter2);

        $manager->persist($plan);

        $manager->flush();
    }
}
