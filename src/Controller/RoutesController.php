<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\EnseignantsRepository;
use App\Repository\DisponibiliteesRepository;
use App\Entity\Enseignants;
use App\Entity\Interventions;
use App\Entity\Matieres;
use App\Entity\Planning;
use App\Entity\Disponibilitees;
use App\Repository\InterventionsRepository;
use App\Repository\MatiereRepository;
use App\Repository\MatieresRepository;
use App\Repository\PlanningRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RoutesController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    function  home(Request $requete, EnseignantsRepository $repoEs){
        $session = $requete->getSession();
        $session->clear();
        $enseignants = $repoEs->findAll();

        return $this->render('pages/home.html.twig',[
            'enseignants' => $enseignants
        ]);
    }

    /**
     * @Route("/home/{id}", requirements={"id" = "\d+"}, name="show_dispo")
     */
    function  dispo(Enseignants $enseignant, EnseignantsRepository $repoEs, DisponibiliteesRepository $repoDispo){
        $enseignants = $repoEs->findAll();
        $dispo = $repoDispo->findBy(array('enseignant' => $enseignant->getId()));

        return $this->render('pages/home-dispos.html.twig',[
            'infoEnseignant' => $enseignant,
            'enseignants' => $enseignants,
            'dispos' => $dispo
        ]);
    }

    /**
     * @Route("/calendar/annee", name="calendar")
     */
    function  calendaryear(){
        return $this->render('pages/calendar-year.html.twig',[
        ]);
    }

    /**
     * @Route("/calendar/annee/mois", name="calendar-month")
     */
    function  calendarmonth(Request $requete, PlanningRepository $repoPla){
        $mois = $requete->request->get('mois');
        $annee = $requete->request->get('annee');
        $session = $requete->getSession();
        $session->set('mois',$mois);
        $session->set('annee',$annee);
        $repoPla = $this->getDoctrine()->getRepository(Planning::class);
        $listeJours = $repoPla->infosMois($mois, $annee);
        $joursdelasemaine = ['lun','mar','mer','jeu','ven','sam','dim'];
        return $this->render('pages/calendar-month.html.twig',[
            'listeJours' => $listeJours,
            'jours' => $joursdelasemaine
        ]);
    }

    /**
     * @Route("/calendar/annee/mois/jours", name="calendar-day")
     */
    function  calendarday(Request $requete, PlanningRepository $repoPla, MatieresRepository $repoMAt, EnseignantsRepository $repoEns){
        $joursdelasemaine = ['lun','mar','mer','jeu','ven','sam','dim'];
        $convertmois = ['Janvier' => '01','Fevrier' => '02','Mars' => '03','Avril' => '04','Mai' => '05','Juin' => '06','Juillet' => '07','Aout' => '08','Septembre' => '09','Octobre' => '10','Novembre' => '11','Décembre' => '12'];

        //Données pour les pages parent et grand parent
        $jours = $requete->request->get('jours');
        $session = $requete->getSession();
        $session->set('jours', $jours);
        $mois = $session->get('mois');
        $annee = $session->get('annee');
        $repoPla = $this->getDoctrine()->getRepository(Planning::class);
        $listeJours = $repoPla->infosMois($mois, $annee);

        //Récupération des interventions
        $planning = $repoPla->findOneBy(array('date' => new \DateTime($annee.'-'.$convertmois[$mois].'-'.$jours)));

        $session->set('planning', $planning);
        $repoMat = $this->getDoctrine()->getRepository(Matieres::class);
        $matieres = $repoMat->findAll();
        $repoEns = $this->getDoctrine()->getRepository(Enseignants::class);
        $enseignants = $repoEns->findAll();

        return $this->render('pages/calendar-day.html.twig',[
            'listeJours' => $listeJours,
            'jours' => $joursdelasemaine,
            'matieres' => $matieres,
            'enseignants' => $enseignants
        ]);
    }

    /**
     * @Route("/saveplanning", name="saveday", methods={"POST"})
     */
    function saveday(Request $requete){
        $convertmois = ['Janvier' => '01','Fevrier' => '02','Mars' => '03','Avril' => '04','Mai' => '05','Juin' => '06','Juillet' => '07','Aout' => '08','Septembre' => '09','Octobre' => '10','Novembre' => '11','Décembre' => '12'];
        $manager = $this->getDoctrine()->getManager();
        $session = $requete->getSession();
        $jours = $session->get('jours');
        $mois = $session->get('mois');
        $annee = $session->get('annee');
        $isNewPlanning = false;

        $idmatierematin =  $requete->request->get("data")["idmatierematin"];
        $idmatiereaprem =  $requete->request->get("data")["idmatiereaprem"];
        $nomintermatin =  $requete->request->get("data")["nomintermatin"];
        $nominteraprem =  $requete->request->get("data")["nominteraprem"];
        $idintervenantmatin =  $requete->request->get("data")["idintervenantmatin"];
        $idintervenantaprem =  $requete->request->get("data")["idintervenantaprem"];
        $idplan =  $requete->request->get("data")["idplan"];

        if($idplan != ""){
            $pla = $manager->getRepository(Planning::class)->find($idplan);
        }else{
            $pla = new Planning();
            $pla->setDate(new \DateTime($annee.'-'.$convertmois[$mois].'-'.$jours));
            $pla->setInterAprem(new Interventions());
            $pla->setInterMatin(new Interventions());
        }

        if($idmatierematin != ""){
            $pla->getInterMatin()->setMatiere($manager->getRepository(Matieres::class)->find($idmatierematin));
        }else{
            // $pla->getInterMatin()->setMatiere(null);
        }
        
        if($nomintermatin != ""){
            $pla->getInterMatin()->setNomInter($nomintermatin);
        }else{
            // $pla->getInterMatin()->setNomInter(null);
        }
        
        if($idintervenantmatin != ""){
            $pla->getInterMatin()->setEnseignant($manager->getRepository(Enseignants::class)->find($idintervenantmatin));
        }else{
            // $pla->getInterMatin()->setEnseignant(null);
        }

        if($idmatiereaprem != ""){
            $pla->getInterAprem()->setMatiere($manager->getRepository(Matieres::class)->find($idmatiereaprem));
        }

        if($nominteraprem != ""){
            $pla->getInterAprem()->setNomInter($nominteraprem);
        }

        if($idintervenantaprem != ""){
            $pla->getInterAprem()->setEnseignant($manager->getRepository(Enseignants::class)->find($idintervenantaprem));
        }

        $manager->persist($pla);
        $manager->flush();

        $isNewPlanning = false;

        return new Response();
    }

    /**
     * @Route("/indisponibilite", name="indispo")
     */
    function indispo(Request $requete, ObjectManager $manager){
        $dispo = new Disponibilitees();

        $form = $this->createFormBuilder($dispo)
                ->add('enseignant', EntityType::class, [
                    'class' => Enseignants::class,
                    'query_builder' => function (EnseignantsRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.nom', 'ASC');
                    },
                    'choice_label' => function ($category) {
                        return $category->getNom().' '.$category->getPrenom();
                    },
                ])
                ->add('date', DateType::class)
                ->add('heureDebut', TimeType::class)
                ->add('heureFin', TimeType::class)
                ->add('commentaire', TextAreaType::class)
                ->add('save', SubmitType::class,[
                    'label' => "Enregistrer l'indisponibilité"
                ])
                ->getForm();

        $form->handleRequest($requete);

        if($form->isSubmitted() && $form->isValid()){
            $manager->persist($dispo);
            $manager->flush();

            return $this->redirectToRoute('show_dispo', ['id' => $dispo->getEnseignant()->getId()]);
        }

        return $this->render('pages/indispo.html.twig',[
            'formDispo' => $form->createView()
        ]);
    }
}