<?php

namespace App\Repository;

use App\Entity\Enseignants;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Enseignants|null find($id, $lockMode = null, $lockVersion = null)
 * @method Enseignants|null findOneBy(array $criteria, array $orderBy = null)
 * @method Enseignants[]    findAll()
 * @method Enseignants[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnseignantsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Enseignants::class);
    }
}
