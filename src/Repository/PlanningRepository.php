<?php

namespace App\Repository;

use App\Entity\Planning;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use \DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Planning|null find($id, $lockMode = null, $lockVersion = null)
 * @method Planning|null findOneBy(array $criteria, array $orderBy = null)
 * @method Planning[]    findAll()
 * @method Planning[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanningRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Planning::class);
    }

    public function infosMois(string $mois, string $annee)
    {
        $joursdelasemaine = ['Mon' => 'lun','Tue' => 'mar','Wed' => 'mer','Thu' => 'jeu','Fri' => 'ven','Sat' => 'sam','Sun' => 'dim'];
        $convertmois = ['Janvier' => '01','Fevrier' => '02','Mars' => '03','Avril' => '04','Mai' => '05','Juin' => '06','Juillet' => '07','Aout' => '08','Septembre' => '09','Octobre' => '10','Novembre' => '11','Décembre' => '12'];
        $mois = $convertmois[$mois];
        
        $liste = array();
        for($i = 0 ; $i <= 31 ; $i++){
            if(checkdate((int)$mois,$i,(int)$annee)){
                $date = new \DateTime($annee.'-'.$mois.'-'.$i);
                $day = $date->format('D');
                array_push($liste,array('numJour' => $i, 'nomJour' => $joursdelasemaine[$day]));
            }
        }

        return $liste;
    }
}
