<?php

namespace App\Repository;

use App\Entity\Disponibilitees;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Disponibilitees|null find($id, $lockMode = null, $lockVersion = null)
 * @method Disponibilitees|null findOneBy(array $criteria, array $orderBy = null)
 * @method Disponibilitees[]    findAll()
 * @method Disponibilitees[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DisponibiliteesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Disponibilitees::class);
    }
}
