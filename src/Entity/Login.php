<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LoginRepository")
 */
class Login
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $role;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Enseignants", cascade={"persist", "remove"})
     */
    private $enseignant;

    /**
     * @ORM\Column(type="integer")
     */
    private $droits;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getEnseigant(): ?self
    {
        return $this->enseignant;
    }

    public function setEnseigant(?self $enseignant): self
    {
        $this->enseignant = $enseignant;

        return $this;
    }

    public function getDroits(): ?int
    {
        return $this->droits;
    }

    public function setDroits(int $droits): self
    {
        $this->droits = $droits;

        return $this;
    }
}
