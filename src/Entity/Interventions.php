<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InterventionsRepository")
 */
class Interventions
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Enseignants", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $enseignant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Matieres", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $matiere;

    /**
     * @ORM\Column(type="string", length=50)
     * @ORM\JoinColumn(nullable=true)
     */
    private $nomInter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEnseignant(): ?Enseignants
    {
        return $this->enseignant;
    }

    public function setEnseignant(Enseignants $enseignant)
    {
        $this->enseignant = $enseignant;

        return $this;
    }

    public function getMatiere(): ?Matieres
    {
        return $this->matiere;
    }

    public function setMatiere(Matieres $matiere)
    {
        $this->matiere = $matiere;

        return $this;
    }

    public function getNomInter(): ?string
    {
        return $this->nomInter;
    }

    public function setNomInter(string $nomInter)
    {
        $this->nomInter = $nomInter;

        return $this;
    }

    // public function getPlanningMatin(): ?Planning
    // {
    //     return $this->planningMatin;
    // }

    // public function setPlanningMatin(?Planning $planningMatin): self
    // {
    //     $this->planningMatin = $planningMatin;

    //     // set (or unset) the owning side of the relation if necessary
    //     $newInterMatin = $planningMatin === null ? null : $this;
    //     if ($newInterMatin !== $planningMatin->getInterMatin()) {
    //         $planningMatin->setInterMatin($newInterMatin);
    //     }

    //     return $this;
    // }

    // public function getPlanningAprem(): ?Planning
    // {
    //     return $this->planningAprem;
    // }

    // public function setPlanningAprem(?Planning $planningAprem): self
    // {
    //     $this->planningAprem = $planningAprem;

    //     // set (or unset) the owning side of the relation if necessary
    //     $newInterAprem = $planningAprem === null ? null : $this;
    //     if ($newInterAprem !== $planningAprem->getInterAprem()) {
    //         $planningAprem->setInterAprem($newInterAprem);
    //     }

    //     return $this;
    // }
}
