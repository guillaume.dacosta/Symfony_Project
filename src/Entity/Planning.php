<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanningRepository")
 */
class Planning
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Interventions", cascade={"persist", "remove"})
     */
    private $interMatin;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Interventions", cascade={"persist", "remove"})
     */
    private $interAprem;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdInter(): ?Interventions
    {
        return $this->idInter;
    }

    public function setIdInter(?Interventions $idInter): self
    {
        $this->idInter = $idInter;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getInterMatin(): ?Interventions
    {
        return $this->interMatin;
    }

    public function setInterMatin(?Interventions $interMatin): self
    {
        $this->interMatin = $interMatin;

        return $this;
    }

    public function getInterAprem(): ?Interventions
    {
        return $this->interAprem;
    }

    public function setInterAprem(?Interventions $interAprem): self
    {
        $this->interAprem = $interAprem;

        return $this;
    }
}
