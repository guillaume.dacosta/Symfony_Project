<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191125181938 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE disponibilitees (id INT AUTO_INCREMENT NOT NULL, enseignant_id INT DEFAULT NULL, date DATETIME NOT NULL, heure_debut TIME NOT NULL, heure_fin TIME NOT NULL, commentaire VARCHAR(255) NOT NULL, INDEX IDX_FE9F56FBE455FCC0 (enseignant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE enseignants (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, prenom VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE interventions (id INT AUTO_INCREMENT NOT NULL, enseignant_id INT NOT NULL, matiere_id INT NOT NULL, nom_inter VARCHAR(50) NOT NULL, UNIQUE INDEX UNIQ_5ADBAD7FE455FCC0 (enseignant_id), UNIQUE INDEX UNIQ_5ADBAD7FF46CD258 (matiere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE login (id INT AUTO_INCREMENT NOT NULL, enseignant_id INT DEFAULT NULL, username VARCHAR(50) NOT NULL, password VARCHAR(50) NOT NULL, role VARCHAR(50) NOT NULL, droits INT NOT NULL, UNIQUE INDEX UNIQ_AA08CB10E455FCC0 (enseignant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matieres (id INT AUTO_INCREMENT NOT NULL, nom_matiere VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE planning (id INT AUTO_INCREMENT NOT NULL, inter_id INT DEFAULT NULL, date DATETIME NOT NULL, heure_debut TIME NOT NULL, date_fin TIME NOT NULL, nom_jours VARCHAR(3) NOT NULL, UNIQUE INDEX UNIQ_D499BFF6E858909E (inter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE disponibilitees ADD CONSTRAINT FK_FE9F56FBE455FCC0 FOREIGN KEY (enseignant_id) REFERENCES enseignants (id)');
        $this->addSql('ALTER TABLE interventions ADD CONSTRAINT FK_5ADBAD7FE455FCC0 FOREIGN KEY (enseignant_id) REFERENCES enseignants (id)');
        $this->addSql('ALTER TABLE interventions ADD CONSTRAINT FK_5ADBAD7FF46CD258 FOREIGN KEY (matiere_id) REFERENCES matieres (id)');
        $this->addSql('ALTER TABLE login ADD CONSTRAINT FK_AA08CB10E455FCC0 FOREIGN KEY (enseignant_id) REFERENCES enseignants (id)');
        $this->addSql('ALTER TABLE planning ADD CONSTRAINT FK_D499BFF6E858909E FOREIGN KEY (inter_id) REFERENCES interventions (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE disponibilitees DROP FOREIGN KEY FK_FE9F56FBE455FCC0');
        $this->addSql('ALTER TABLE interventions DROP FOREIGN KEY FK_5ADBAD7FE455FCC0');
        $this->addSql('ALTER TABLE login DROP FOREIGN KEY FK_AA08CB10E455FCC0');
        $this->addSql('ALTER TABLE planning DROP FOREIGN KEY FK_D499BFF6E858909E');
        $this->addSql('ALTER TABLE interventions DROP FOREIGN KEY FK_5ADBAD7FF46CD258');
        $this->addSql('DROP TABLE disponibilitees');
        $this->addSql('DROP TABLE enseignants');
        $this->addSql('DROP TABLE interventions');
        $this->addSql('DROP TABLE login');
        $this->addSql('DROP TABLE matieres');
        $this->addSql('DROP TABLE planning');
    }
}
