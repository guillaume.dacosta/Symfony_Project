<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191202143722 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE planning DROP FOREIGN KEY FK_D499BFF6E858909E');
        $this->addSql('DROP INDEX UNIQ_D499BFF6E858909E ON planning');
        $this->addSql('ALTER TABLE planning CHANGE inter_id inter_matin_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE planning ADD CONSTRAINT FK_D499BFF67EFAB071 FOREIGN KEY (inter_matin_id) REFERENCES interventions (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D499BFF67EFAB071 ON planning (inter_matin_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE planning DROP FOREIGN KEY FK_D499BFF67EFAB071');
        $this->addSql('DROP INDEX UNIQ_D499BFF67EFAB071 ON planning');
        $this->addSql('ALTER TABLE planning CHANGE inter_matin_id inter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE planning ADD CONSTRAINT FK_D499BFF6E858909E FOREIGN KEY (inter_id) REFERENCES interventions (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D499BFF6E858909E ON planning (inter_id)');
    }
}
